package utils_test

import (
	"github.com/h2non/gock"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.com/mohammed-strive/demystdata/utils"
)

var _ = Describe("Http", func() {
	Describe("When HTTP call is successful", func() {
		It("should return valid data from the response", func() {
			defer gock.Off()
			gock.New("https://jsonplaceholder.typicode.com/todos/").
				Get("20").
				Reply(200).
				JSON(map[string]interface{}{"id": 1,
					"title":     "Test title",
					"completed": false,
					"userId":    2,
				})

			resp := utils.HttpGet("https://jsonplaceholder.typicode.com/todos/20")
			Expect(resp).ToNot(BeNil())
			Expect(resp.Res.Id).To(Equal(1))
			Expect(resp.Res.Completed).To(BeFalse())
			Expect(resp.Res.Title).To(Equal("Test title"))
		})
	})

	Describe("When HTTP call fails", func() {
		It("should return valid data from the response", func() {
			defer gock.Off()
			gock.New("https://jsonplaceholder.typicode.com/todos/").
				Get("20").
				Reply(404)

			resp := utils.HttpGet("https://jsonplaceholder.typicode.com/todos/20")
			Expect(resp.Err).ToNot(BeNil())
		})
	})
})
