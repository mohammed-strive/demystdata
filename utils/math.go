package utils

func IsEven(val int) bool {
	return val%2 == 0
}
