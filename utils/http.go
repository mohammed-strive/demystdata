package utils

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"

	"gitlab.com/mohammed-strive/demystdata/types"
)

func HttpGet(urlString string) types.Data {
	if _, err := url.ParseRequestURI(urlString); err != nil {
		return types.Data{Err: err}
	}

	resp, err := http.Get(urlString)
	if err != nil {
		return types.Data{Err: err}
	}
	defer resp.Body.Close()
	data, err := io.ReadAll(resp.Body)
	if err != nil {
		fmt.Printf("%v\n", err)
		return types.Data{Err: err}
	}
	var res types.Result
	err = json.Unmarshal(data, &res)
	if err != nil {
		return types.Data{Err: err}
	}
	return types.Data{Res: res}
}
