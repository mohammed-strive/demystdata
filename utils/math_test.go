package utils_test

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"gitlab.com/mohammed-strive/demystdata/utils"
)

var _ = Describe("Math", func() {
	It("should return true when input is 100", func() {
		val := 100
		Expect(utils.IsEven(val)).To(BeTrue())
	})

	It("should return false when input is 99", func() {
		val := 99
		Expect(utils.IsEven(val)).To(BeFalse())
	})
})
