package functions

import (
	"context"

	"gitlab.com/mohammed-strive/demystdata/types"
)

func Take(ctx context.Context, valSteam <-chan types.Data, times int) <-chan types.Data {
	stream := make(chan types.Data)
	go func() {
		defer close(stream)
		for i := 0; i < times; i++ {
			select {
			case <-ctx.Done():
				return
			case stream <- <-valSteam:
			}
		}
	}()

	return stream
}
