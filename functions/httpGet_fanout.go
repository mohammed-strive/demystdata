package functions

import (
	"context"

	"gitlab.com/mohammed-strive/demystdata/types"
	"gitlab.com/mohammed-strive/demystdata/utils"
)

const TODO_URL = "https://jsonplaceholder.typicode.com/todos/"

func HttpGetFanOut(ctx context.Context, data types.Data) <-chan types.Data {
	stream := make(chan types.Data)

	go func() {
		defer close(stream)
		select {
		case <-ctx.Done():
			return
		default:
			newData := utils.HttpGet(TODO_URL + data.Id)
			select {
			case <-ctx.Done():
				return
			case stream <- newData:
			}
		}
	}()

	return stream
}
