package functions

import (
	"context"
	"fmt"

	"gitlab.com/mohammed-strive/demystdata/types"
)

func PrintResult(ctx context.Context, inputStream <-chan types.Data) {
	for inp := range inputStream {
		select {
		case <-ctx.Done():
			return
		default:
			if inp.Err == nil {
				fmt.Printf("%s: %v\n", inp.Res.Title, inp.Res.Completed)
			}
		}
	}
}
