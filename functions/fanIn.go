package functions

import (
	"context"
	"sync"

	"gitlab.com/mohammed-strive/demystdata/types"
)

func FanIn(ctx context.Context, channels ...<-chan types.Data) <-chan types.Data {
	var wg sync.WaitGroup
	channelStream := make(chan types.Data)

	multiplex := func(channel <-chan types.Data) {
		defer wg.Done()
		for val := range channel {
			select {
			case <-ctx.Done():
				return
			case channelStream <- val:
			}
		}
	}

	wg.Add(len(channels))
	for _, channel := range channels {
		go multiplex(channel)
	}

	go func() {
		wg.Wait()
		close(channelStream)
	}()

	return channelStream
}
