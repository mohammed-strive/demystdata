package functions_test

import (
	"context"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.com/mohammed-strive/demystdata/functions"
	"gitlab.com/mohammed-strive/demystdata/types"
)

var _ = Describe("CheckEven", func() {
	It("should return a channel which has data having only even Ids", func() {
		ctx := context.TODO()
		getInput := func() <-chan types.Data {
			stream := make(chan types.Data)
			go func() {
				defer close(stream)
				for v := range 10 {
					stream <- types.Data{TodoId: v}
				}
			}()

			return stream
		}

		inpStream := getInput()
		var output []types.Data

		for val := range functions.CheckEven(ctx, inpStream) {
			output = append(output, val)
		}

		Expect(len(output)).To(Equal(4))
	})
})
