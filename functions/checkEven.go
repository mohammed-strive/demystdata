package functions

import (
	"context"

	"gitlab.com/mohammed-strive/demystdata/types"
	"gitlab.com/mohammed-strive/demystdata/utils"
)

func CheckEven(ctx context.Context, inputStream <-chan types.Data) <-chan types.Data {
	stream := make(chan types.Data)
	go func() {
		defer close(stream)
		for inp := range inputStream {
			if utils.IsEven(inp.TodoId) && inp.TodoId != 0 {
				select {
				case <-ctx.Done():
					return
				case stream <- inp:
				}
			}
		}
	}()

	return stream
}
