package functions

import (
	"context"
	"strconv"

	"gitlab.com/mohammed-strive/demystdata/types"
)

func ConvertToInt(ctx context.Context, rawStream <-chan types.Data) <-chan types.Data {
	stream := make(chan types.Data)
	go func() {
		defer close(stream)

		for rawVal := range rawStream {
			val, err := strconv.Atoi(rawVal.Id)
			select {
			case <-ctx.Done():
				return
			case stream <- types.Data{TodoId: val, Id: rawVal.Id, Err: err}:
			}
		}
	}()

	return stream
}
