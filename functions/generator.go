package functions

import (
	"context"

	"gitlab.com/mohammed-strive/demystdata/types"
)

func Generator(ctx context.Context, val ...string) <-chan types.Data {
	stream := make(chan types.Data)
	go func() {
		defer close(stream)
		for _, val := range val {
			select {
			case <-ctx.Done():
				return
			case stream <- types.Data{Id: val}:
			}
		}
	}()

	return stream
}
