## Demystdata project

### Instructions to run
- #### To build the binary
    - Clone the project
    - Ensure `GOPATH`, `GOROOT` and `GOBIN` are set.
    - Navigate to the cloned repo.
    - Run `go mod tidy`
    - Finally Run `go build -o ./bin/` to build the binary into the `bin` folder.

- #### To run the binary
    - Clone the project
    - Navigate to the cloned repo.
    - Run `./bin/demystdata 1 2 3 4 ...`
