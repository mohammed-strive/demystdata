package main

import (
	"context"
	"os"

	fn "gitlab.com/mohammed-strive/demystdata/functions"
	"gitlab.com/mohammed-strive/demystdata/types"
)

func main() {
	ctx := context.Background()
	inputStream := fn.Take(ctx, fn.CheckEven(ctx, fn.ConvertToInt(ctx, fn.Generator(ctx, os.Args[1:]...))), 20)

	var results []<-chan types.Data
	for val := range inputStream {
		results = append(results, fn.HttpGetFanOut(ctx, val))
	}

	fn.PrintResult(ctx, fn.FanIn(ctx, results...))
}
