package types

type Data struct {
	Res    Result
	Err    error
	Id     string
	TodoId int
}
